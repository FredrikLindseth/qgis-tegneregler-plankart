# QGIS-tegneregler-plankart

Tegneregler for QGIS til digitale plankart i Norge, som spesifisert i  [Nasjonal produktspesifikasjon for arealplan og digitalt planregister Del 2 – Spesifikasjon for tegneregler](https://www.regjeringen.no/no/tema/plan-bygg-og-eiendom/plan--og-bygningsloven/plan/veiledning-om-planlegging/plankartsiden/npad/id2361191/)

Tegnereglene inneholder alle arealformålene for kommuneplan (eksisterende og nytt arealformål) og reguleringsplan, så vel som hensynssoner. Disse ulike tegnereglene har hver sin separate stil-fil.

I tillegg til de standard kodene og områdene tegnes `arealformål=1130` (sentrumsformål) og `beskrivelse=sentrumskjerne` litt mørkere enn vanlig sentrumsformål for å framheve dette.

Navnet på arealformålet tegnes ikke og flater, linjer og punkter mangler foreløpig.
